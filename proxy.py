#! /bin/python3
import ssl
import http.client as client
import http.server as server
import socketserver
import socket
import os

elastic_host = os.environ.get('ELASTIC_HOST')
if not elastic_host:
    elastic_host = 'localhost'
elastic_port = os.environ.get('ELASTIC_PORT')
if not elastic_port:
    elastic_port = 9200
PORT = 5000
context = ssl.SSLContext(ssl.PROTOCOL_TLS_SERVER)
context.load_cert_chain("/app/proxy.crt","/app/proxy.key")


class HTTPHandler(server.BaseHTTPRequestHandler):
    protocol_version = 'HTTP/1.1'
    def do_POST(self):
        self.do_GET()
    def do_PUT(self):
        self.do_GET()
    def do_HEAD(self):
        self.do_GET()
    def do_DELETE(self):
        self.do_GET()
    def do_CONNECT(self):
        self.do_GET()
    def do_OPTIONS(self):
        self.do_GET()
    def do_TRACE(self):
        self.do_GET()
    def do_PATCH(self):
        self.do_GET()

    def do_GET(self):
        conn = client.HTTPConnection(elastic_host, elastic_port)
        headers_in = self.headers.items()[1:]
        conn.putrequest(self.command, self.path, skip_accept_encoding=True)
        for h in headers_in:
            conn.putheader(h[0],h[1])
        conn.endheaders()
        try:
            len = int(self.headers.get("content-length"))
            body = self.rfile.read(len)
            conn.send(body)
        except:
            None
        response = conn.getresponse()
        re = response.read()
        self.send_response(response.status,response.reason)
        headers_out = response.getheaders()
        for h in headers_out:            
            self.send_header(h[0],h[1])
        self.end_headers()
        self.wfile.write(re)


httpd = server.ThreadingHTTPServer(("", PORT), HTTPHandler)
httpd.socket = ssl.wrap_socket(httpd.socket, keyfile="./proxy.key", certfile="./proxy.crt", server_side=True)
httpd.serve_forever()
