FROM python:3
RUN mkdir /app
WORKDIR /app
ADD . /app/

ENTRYPOINT ["python", "/app/proxy.py"]
